# Fahrrad

***Wunschliste*** von [KubikPixel](https://thunix.net/~kubikpixel/)

## Fahrrad

1. [Standpumpe](https://www.veloplus.ch/AlleProdukte/Pumpen/Standpumpe/FoeHNStandpumpefuerdieWerkstattvonBETO.aspx) mit Manometer
1. [Komoot Komplett Paket](https://www.komoot.de/product/buy?region=complete-package)
2. ~~[Satteltasche](https://www.veloplus.ch/AlleProdukte/Taschen/Satteltaschen/MORITZLISatteltaschevonVELOPLUS.aspx) für Flickzeug~~
3. ~~[Multitool](https://www.veloplus.ch/AlleProdukte/Werkstatt/Touren-Werkzeug/MINI20PROMultiwerkzeuggoldvonTOPEAK.aspx) für's Velo :wrench:~~
4. ~~[Minipumpe](https://www.veloplus.ch/AlleProdukte/Pumpen/MinipumpefuerMTBTour/MINIDUALDXII2-WegMinipumpevonTOPEAK.aspx) für unterwegs~~
4. ~~[Flick-Set](https://www.veloplus.ch/AlleProdukte/Reifen/VeloFlickzeugPannenschutz/AIRFIXmaxiFlick-SetmitReifenhebervonVELOPLUS.aspx) für unterwegs~~
4. [Velounterhose](https://www.veloplus.ch/AlleProdukte/BekleidungMaenner/Unterwaesche/BENITOHerren-VelounterhoseschwarzvonGONSO.aspx) Grösse M
5. [Regenjacke](https://www.probikeshop.ch/de/ch/jacke-gore-bike-wear-element-gore-tex-paclite-neon-gelb-2015/111110.html) gegen Regen
6. [Trinkschlauch](https://www.amazon.de/Source-2031160200-Convertube-Blau/dp/B001GHIPDA?psc=1&SubscriptionId=AKIAIT5QD4SNY5Q734UQ&tag=vt-shop-21&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B001GHIPDA&keywords=trinkschlauch%20f%C3%BCr%20pet%20flaschen) für gängige PET Flaschen
5. [Bidonhalter](https://www.sportxx.ch/de/p/462904100000/crosswave-cd-75) für die Trinkflasche
6. [Ventiladapter](https://www.veloplus.ch/AlleProdukte/Pumpen/ErsatzteilePumpenVentiladapterZubehoer/Ventil-undFelgen-Adapter.aspx) Presta ==> Schrader / Velo ==> Auto
10. [Winterreifen](https://www.schwalbe.com/de/offroad-reader/racing-ralph.html) gibt es auch für Velos
11. [Kettenschloss](https://www.veloplus.ch/AlleProdukte/KetteKettenschutzRiemenantrieb/MISSINGLINKKettenverschlussglied9fachvonKMC.aspx) 9fach
5. [Rücklicht](https://www.knog.com.au/bike-lights/rear-lights/blinder-road-r70.html) mit Leistung
6. [Skateschuhe](https://www.zalando.ch/etnies-marana-sneaker-low-et112a02g-q18.html) geben guten Grip
6. [Trinkrucksack](https://www.veloplus.ch/AlleProdukte/RucksackHuefttasche/Trinkrucksaecke/ROGUETrinkrucksack2lschwarzvonCAMELBAK.aspx) für längere Touren
7. [Bikeshirt](https://www.pirate-shop.biz/de/fahrradtrikots-kurzer-arm/pirate-trikot-china-schwarz) kurzärmlig, Grösse M
8. [Bikeshirt](https://www.pirate-shop.biz/de/fahrradtrikots-langer-arm/pirate-trikot-l-a-rising-sun) langärmlig, Grösse M
9. [Bikeshirt](https://www.klingklang.de/index.php?main_page=product_info&cPath=3&products_id=6&zenid=kbvju93rr4km2oarvggla88us5) kurzärmlig, Grösse M
12. ~~[Oberrohrtasche](https://www.veloplus.ch/AlleProdukte/Taschen/Rahmentaschen/HUMPBAGOberrohrtaschevonVELOPLUS.aspx) für Handy und mehr Stauraum~~
9. ~~[Schlauchtuch](http://www.sportxx.ch/de/bekleidung-herren/accessoires/neck-tube/pp.477067799920?selectvariant) als Schal~~
9. [Velo Pflegeset](https://www.veloplus.ch/AlleProdukte/ReinigungPflege/PETRUSVelopflegevonVELOPLUS/PETRUSVELOPFLEGE-SETvonVELOPLUS.aspx) für die Zierde
12. [Drehmomentschlüssel](https://www.galaxus.ch/de/s4/product/jet-tools-z-25-14-5-25nm-drehmomentschluessel-259868?tagIds=426-152) für heikle Montagen
11. [Sh!t Kit](https://www.pottypacks.com/product-page/box-of-sh-t-kits), denn alle müssen mal... :poop:
21. [Marshguard](https://www.veloplus.ch/AlleProdukte/GabelFedergabel/FedergabelSpritzschutzSchutz/marshguard-splashguard-federgabelspritzschutz-von-veloplus.aspx) Spritzschutz

## Reiseapotheke

- [Mückenstift](https://www.amazon.de/gp/product/B003CMKQTS/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=carolin0e-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B003CMKQTS&linkId=7f1b96386650d32093a03c1c0fbb0953)
- [Wundreinigungstücher](https://www.adlershop.ch/p/2460/dermaplast-clean-10-wundreinigungstuecher)
- [Wundgel](https://www.adlershop.ch/p/4912/octenisept-wundgel-20ml)
- [Sportsalbe](https://www.adlershop.ch/p/9450/dul-x-gel-sport-relax-125ml)
- [Netzverband](https://www.adlershop.ch/p/13952/retelast-netzverband-2-10m)
- [Wundverschluss](https://www.adlershop.ch/p/8013/3m-steri-strip-6x100mm-weiss-verstaerkt-10-stueck)
- [Sitzcreme](http://www.ilon-protect-salbe.de/sport/einsatzgebiete/radsport-gesaesssalbe-fuer-fahrradfahrer/)
- [Sonnencreme 50+](https://www.adlershop.ch/p/172113/biarritz-sonnencreme-gesicht-lsf-50-dispenser-50ml)
- [Insektenschutz](https://www.adlershop.ch/p/369/anti-brumm-forte-insektenschutz-spray-75ml)
- [Alka Seltzer](https://www.adlershop.ch/p/230/alka-seltzer-20-brausetabletten)
- ...

## Tech

1. [Solarpanel](https://www.xtpower.de/XTPower-Solarpanel-SP13-mit-13-Watt-Solar-Netzteil-mobil-2x-USB-Faltbar-Monokristallin) als Netzteil
2. [Bangle.js](https://shop.espruino.com/banglejs) Open Source Smart Watch
3. [Bike Computer](http://sigmasport.com/en/produkte/fahrrad-computer/gps/pure/pure-gps)
4. [Fitnesstracker](https://www.polar.com/ch-de/products/pro/V800) für Velo und Alltag

## Outdoor

1. Tarp
2. Plastikplane
3. Feuerstahl
4. Spork
5. Outdoor Kocher
6. ...
