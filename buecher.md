# Wishlist

***Wunschliste*** von [KubikPixel](https://thunix.net/~kubikpixel/):

## Bücher

|Erhalten|Titel|AutorIn|Sprache|ISBN|Bestellen|
|---|---|---|---|---|---|
|-|GRM - Brainfuck|Sibylle Berg|Deu.|978-3-462-05143-8|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/A1052817730)|
|-|Darknet - Waffen, Drogen, Whistleblower|Stefan Mey|Deu.||[C.H. Beck](https://www.chbeck.de/mey-darknet/product/32823389)|
|-|Ein anderes Jetzt|Yanis Varoufakis|Deu.|978-3-95614-459-2|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/A1060570142)|
|-|Der Fluch des Reichtums|Tom Burgis|Deu.|978-3-86489-906-5|[Buch Kompliezen](https://www.buchkomplizen.de/der-fluch-des-reichtums.html)|
|-|Kleptopia|Tom Burgis|Deu.|978-3-86489-326-1|[Buch Kompliezen](https://www.buchkomplizen.de/kleptopia.html)|
|-|Wirtschaft Hacken|Uwe Lübbermann|Deu.|978-3-96317-233-5|[Büchner](https://www.buechner-verlag.de/buch/wirtschaft-hacken/)|
|-|Die Macht der Plattformen|Michael Seemann|Deu.|978-3-96289-075-9|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID146367812.html)|
|-|True Facts|Katharina Nocun, Pia Lamberty|Deu.|978-3-86995-114-0|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID151058295.html)|
|-|Hinter der Fassade der Wirklichkeit|Michael Schwaiger|Deu.|978-3-85476-545-5|[Lehmanns](https://www.lehmanns.ch/shop/literatur/39003506-9783854765455-hinter-der-fassade-der-wirklichkeit)|
|-|Das Zeitalter des Überwachungskapitalismus|Shoshana Zuboff|Deu.|978-3-593-50930-3|[Orell Füssli](https://www.orellfuessli.ch/shop/home/suggestartikel/ID115699024.html)|
|-|Picknick am Wegesrand|Arkadi & Boris Strugatzki|Deu.|978-3-518-37170-1|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID2902327.html)|
|-|Exodus 2727 - Die letzte Archeriot|Thariot|Deu.|978-3-596-70447-7|[S. Fischer Verlage](https://www.fischerverlage.de/buch/thariot-exodus-2727-die-letzte-arche-9783596704477)|
|-|Exodus 9414 - Der dunkelste Tag|Thariot|Deu.|978-3-596-70038-7|[S. Fischer Verlage](https://www.fischerverlage.de/buch/thariot-exodus-9414-der-dunkelste-tag-9783596700387)|
|-|Linux-Server|Dirk Deimeke u.v.m.|Deu.|978-3-8362-8088-4|[Rheinwerk](https://www.rheinwerk-verlag.de/linux-server-das-umfassende-handbuch/)|
|-|Hacking & Security|Michael Kofler u.v.m.|Deu.|978-3-8362-7191-2|[Rheinwerk](https://www.rheinwerk-verlag.de/hacking-und-security-das-umfassende-handbuch/)|
|-|Der Fall Julian Assange|Nils Melzer|Deu.|978-3-492-07076-8|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID149944070.html)|
|-|Stolen|Grace Blakeley|Deu.|🤷|[Jacobin](https://shop.jacobin.de/deal/stolen/bestellen)|
|-|Dann haben die halt meine Daten. Na und?!|Klaudia Zotzmann-Koch|Deu.|978-3-7519-8149-1|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID150775715.html)|
|-|Die Datendiktatur|Brittany Kaiser|Deu.|978-3-95967-390-7|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID144630849.html)|
|-|Konformistische Rebellen|Andreas Stahl u.v.m.|Deu.|978-3-95732-433-7|[Verbrecherverlag](https://www.verbrecherverlag.de/book/detail/1025)|
|-|Permanent Record|Edward Snowden|Deu.|978-3-10-397482-9|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID33776140.html)|
|-|Daten, Drohnen, Disziplin|Zygmunt Bauman, David Lyon|Deu.|978-3-518-12667-7|[Orell Füssle](https://www.orellfuessli.ch/shop/home/artikeldetails/ID33776140.html)|
|-|NSA - Nationales Sicherheits-Amt|Andreas Eschbach|Deu.|978-3-7857-2625-9|[Orell Füssle](https://www.orellfuessli.ch/shop/home/artikeldetails/ID121090320.html)|
|-|Life after Privacy|Firmin DeBrabander|Eng.|978-1-108-81191-0|[Orell Füssle](https://www.orellfuessli.ch/shop/home/artikeldetails/ID146761307.html)|
|X|Nomaden der Arbeit|Jessica Bruder|Deu.|978-3-89667-630-6|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID140475343.html)|
|X|Kuckucksei|Clifford Stoll|Deu.|978-3-596-30722-7|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID44198017.html)|
|X|VERAX - Das Experiment|Jörg Benne|Deu.|978-3-96188-008-9|[Orell Füssli](https://www.orellfuessli.ch/shop/home/artikeldetails/ID100262512.html)|

## Bastelkram

|Erhalten|HW|Type|Web|
|---|---|---|---|
|-|Raspberry Pi|Zero 2 W|[raspberrypi.com](https://www.raspberrypi.com/products/raspberry-pi-zero-2-w/)|
